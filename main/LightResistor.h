#ifndef LightResistor_h
#define LightResistor_h

#include "Arduino.h"

class LightResistor
{
  public:
    LightResistor(int pin);
    float percent();
  private:
    int _pin;
};

#endif