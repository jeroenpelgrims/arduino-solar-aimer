#include "Servo.h"
#include "LightResistor.h"

LightResistor topRight(A0);
LightResistor bottomRight(A1);
LightResistor bottomLeft(A2);
LightResistor topLeft(A3);
Servo horizontal;
Servo vertical;
float TOLERANCE = 0.08f;
int MOVE_STEP = 1;

void servoUpdate(Servo servo, int relativeValue) {
    int current = servo.read();
    int newValue = current + relativeValue;
    servo.write(min(max(newValue, 0), 180));
}

void setup()
{
    Serial.begin(9600);
    horizontal.attach(12);
    vertical.attach(11);

    Serial.println("Resetting servos and pausing for 2 sec");
    vertical.write(90);
    horizontal.write(90);
    delay(2000);
    Serial.println("Starting seeking");
}

void loop()
{
    float avgTop = ( topLeft.percent() + topRight.percent() ) / 2;
    float avgBottom = ( bottomLeft.percent() + bottomRight.percent() ) / 2;
    float avgLeft = ( topLeft.percent() + bottomLeft.percent() ) / 2;
    float avgRight = ( topRight.percent() + bottomRight.percent() ) / 2;

    float diffHorizontal = avgLeft - avgRight;
    float diffVertical = avgTop - avgBottom;

    if (abs(diffHorizontal) > TOLERANCE) {
        if (diffHorizontal > 0) {
            Serial.println("LEFT");
            servoUpdate(horizontal, MOVE_STEP);
        } else {
            Serial.println("RIGHT");
            servoUpdate(horizontal, -MOVE_STEP);
        }
    }

    if (abs(diffVertical) > TOLERANCE) {
        if (diffVertical > 0) {
            Serial.println("UP");
            servoUpdate(vertical, -MOVE_STEP);
        } else {
            Serial.println("DOWN");
            servoUpdate(vertical, MOVE_STEP);
        }
    }

    //printDebugValues(diffHorizontal, diffVertical);
    delay(20);
}

void printDebugValues(float diffHorizontal, float diffVertical) {
    Serial.print(topLeft.percent());
    Serial.print(" ");
    Serial.print(topRight.percent());
    Serial.print(" > ");
    Serial.println(diffHorizontal);
    Serial.print(bottomLeft.percent());
    Serial.print(" ");
    Serial.println(bottomRight.percent());
    Serial.println("  v");
    Serial.print("  ");
    Serial.println(diffVertical);
    Serial.println();
}