#include "Arduino.h"
#include "LightResistor.h"

LightResistor::LightResistor(int pin)
{
  pinMode(pin, INPUT);
  _pin = pin;
}

float LightResistor::percent()
{
    return analogRead(_pin) / 1024.0;
}